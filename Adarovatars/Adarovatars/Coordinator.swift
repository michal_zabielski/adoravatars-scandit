//
//  Coordinator.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation
import UIKit

enum Transition {
    case initial
    case networking
}

protocol CoordinatorType {
    func performTransition(transition: Transition)
}

class Coordinator: CoordinatorType {
    var navigationController: UINavigationController = UINavigationController()
    var downloaderService: DownloaderService
    
    init(){
        let imageDownloader = ImageDownloader(link: Constants.link, requestTimeout: Constants.requestTimeout)
        downloaderService = DownloaderService(imageDownloader: imageDownloader)
        downloaderService.startDownloading(items: Constants.names)
    }
    
    func performTransition(transition: Transition){
        switch (transition) {
        case .initial:
            let iVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AvatarsCollectionViewController") as! AvatarsCollectionViewController
            let viewModel = AvatarsViewModel(downloaderService: self.downloaderService)
            viewModel.coordinator = self
            iVC.viewModel = viewModel
            viewModel.viewDelegate = iVC
            viewModel.reloadAll()
            navigationController.pushViewController(iVC, animated: true)
        case .networking:
            let iVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NetworkViewController") as! NetworkTableViewController
            let viewModel = NetworkViewModel(downloaderService: self.downloaderService)
            viewModel.viewDelegate = iVC
            iVC.viewModel = viewModel
            viewModel.coordinator = self
            navigationController.pushViewController(iVC, animated: true)
        }
    }
}
