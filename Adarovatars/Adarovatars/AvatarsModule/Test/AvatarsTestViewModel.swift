//
//  AvatarsTestViewModel.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation
import UIKit.UIImage

class AvatarsTestViewModel: AvatarsViewModelType {
    var coordinator: CoordinatorType?
    var items: [AvatarCellState] = []
    weak var viewDelegate: AvatarsViewDelegate? {
        didSet {
            viewDelegate?.reloadAll()
        }
    }
    
    init(){
        reloadAll()
    }
    
    func reloadAll() {
        items = []
        
        let loaded = Constants.names[1...10].map { name in
            AvatarCellState.loaded(name: name, image: UIImage())
        }
        
        let loading = Constants.names[11...15].map { name in
            AvatarCellState.loading(name: name)
        }
        
        let empty = Constants.names[16...20].map { name in
            AvatarCellState.empty(name: name)
        }
        
        items.append(contentsOf: loaded)
        items.append(contentsOf: loading)
        items.append(contentsOf: empty)
        
        viewDelegate?.reloadAll()
    }
    
    func networkButtonClicked() {
        coordinator?.performTransition(transition: .networking)
    }
    
}
