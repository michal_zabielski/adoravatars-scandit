//
//  AvatarsModuleTypes.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation
import UIKit.UIImage

enum AvatarCellState {
    case empty(name: String)
    case loading(name: String)
    case loaded(name: String, image: UIImage)
}

protocol AvatarsViewDelegate: class {
    func reloadAll()
    func updateAt(index: Int)
}

protocol AvatarsViewModelType {
    var items: [AvatarCellState] { get }
    var viewDelegate: AvatarsViewDelegate? { get set }
    func reloadAll()
    
    func networkButtonClicked()
}
