//
//  AvatarCollectionViewCell.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import UIKit

class AvatarCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var backgroundMask: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
 
    override func prepareForReuse() {
        imageView.isHidden = true
        lblName.text = "--"
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true;
        
        makeRound(view: imageView)
        makeRound(view: backgroundMask)

    }
    
    func makeRound(view: UIView){
        view.layer.masksToBounds = true
        view.layer.cornerRadius = view.frame.size.height / 2
        view.contentMode = .scaleAspectFill
        view.layoutIfNeeded()
    }

    
    func setup(state: AvatarCellState){
        prepareForReuse()
        switch (state){
        case .empty(let name):
            lblName.text = name
        case .loading(let name):
            lblName.text = name
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
        case .loaded(let name, let image):
            lblName.text = name
            imageView.image = image
            imageView.isHidden = false
        }
    }
}
