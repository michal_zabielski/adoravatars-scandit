//
//  AvatarsViewModel.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation
import UIKit.UIImage

class AvatarsViewModel: AvatarsViewModelType {
    var coordinator: CoordinatorType?
    var items: [AvatarCellState] = []
    var downloaderService: DownloaderService
    weak var viewDelegate: AvatarsViewDelegate? {
        didSet {
            viewDelegate?.reloadAll()
        }
    }
    
    init(downloaderService: DownloaderService){
        self.downloaderService = downloaderService
        reloadAll()
    }
    
    func indexOfItem(named: String) -> Int? {
        return items.index(where: {
            switch ($0){
            case .empty(let name):
                return name == named
            case .loaded(let name, _):
                return name == named
            case .loading(let name):
                return name == named
            }
        })
    }
    
    func reloadAll() {
        items = downloaderService.itemsState.map { item in
            AvatarCellState.from(downloadResult: item)
        }
        viewDelegate?.reloadAll()
        
        downloaderService.addListener { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .failure(let name):
                if let index = strongSelf.indexOfItem(named: name) {
                    strongSelf.items[index] = .empty(name: name)
                    strongSelf.viewDelegate?.updateAt(index: index)
                }
            case .image(let name, let image):
                if let index = strongSelf.indexOfItem(named: name) {
                    strongSelf.items[index] = .loaded(name: name, image: image)
                    strongSelf.viewDelegate?.updateAt(index: index)
                }
            case .inProgress(_):
                break //do nothing
            }
        }
    }
    
    func networkButtonClicked() {
        coordinator?.performTransition(transition: .networking)
    }
}

private extension AvatarCellState {
    static func from(downloadResult: DownloadResult) -> AvatarCellState{
        switch(downloadResult) {
        case .failure(let name):
            return .empty(name: name)
        case .image(let name, let image):
            return .loaded(name: name, image: image)
        case .inProgress(let name):
            return .loading(name: name)
        }
    }
}
