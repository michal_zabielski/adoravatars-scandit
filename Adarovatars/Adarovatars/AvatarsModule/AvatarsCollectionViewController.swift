//
//  AvatarsCollectionViewController.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import UIKit

private let reuseIdentifier = "AvatarCell"

class AvatarsCollectionViewController: UICollectionViewController, AvatarsViewDelegate {
    var viewModel: AvatarsViewModelType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func networkButtonClicked(_ sender: Any) {
        viewModel?.networkButtonClicked()
    }
    
    func reloadAll() {
        self.collectionView?.reloadData()
    }
    
    func updateAt(index: Int) {
        self.collectionView?.reloadItems(at: [IndexPath(row: index, section: 0)])
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.items.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        if let cell = cell as? AvatarCollectionViewCell,
           let item = viewModel?.items[indexPath.row] {
            cell.setup(state: item)
        }
        
        return cell
    }
}
