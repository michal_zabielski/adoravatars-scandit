//
//  DownloaderService.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation
import UIKit.UIImage

class DownloaderService: DownloaderServiceType {
    var listeners: [DownloaderServiceListenerBlock] = []
    var internalItems: [String: DownloadResult] = [:]
    var itemsInProgress: Int {
        return itemsState.filter {
            switch $0 {
            case .inProgress(name: _):
                return true
            default:
                return false
            }
        }.count
    }
    var itemsState: [DownloadResult] {
        return Array(internalItems.values)
    }
    var imageDownloader: ImageDownloaderType
    
    init(imageDownloader: ImageDownloaderType) {
        self.imageDownloader = imageDownloader
    }
    
    //TODO: listeners cannot be removed. it's huge weakness to fix
    
    func addListener(block: @escaping DownloaderServiceListenerBlock) {
        listeners.append(block)
    }
    
    func sendToListeners(result: DownloadResult){
        for l in listeners {
            l(result)
        }
    }
    
    func clearData() {
        internalItems = [:]
    }
    
    func startDownloading(items: [String]) {
        clearData()
        for name in items {
            internalItems[name] = DownloadResult.inProgress(name: name)
        }
        for name in items {
            imageDownloader.download(name: name){ [weak self] result in
                guard let strongSelf = self else { return }
                strongSelf.internalItems[name] = result
                strongSelf.sendToListeners(result: result)
            }
        }
    }
}
