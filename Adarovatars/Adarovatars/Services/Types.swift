//
//  Types.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation

protocol ImageDownloaderType {
    func download(name: String, closure: @escaping (DownloadResult) -> Void)
}

typealias DownloaderServiceListenerBlock = (_ result: DownloadResult) -> Void

protocol DownloaderServiceType {
    var itemsState: [DownloadResult] { get }
    func addListener(block: @escaping DownloaderServiceListenerBlock)
    func startDownloading(items: [String])
}
