//
//  ImageDownloader.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation
import UIKit.UIImage

class ImageDownloader: ImageDownloaderType {
    let session: URLSession
    let link: URL
    
    init(link: URL, requestTimeout: TimeInterval){
        self.link = link
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .returnCacheDataElseLoad
        configuration.timeoutIntervalForRequest = requestTimeout
        configuration.timeoutIntervalForResource = requestTimeout
        session = URLSession(configuration: configuration)
    }
    
    func download(name: String, closure: @escaping (DownloadResult) -> Void){
        var url = self.link
        url.appendPathComponent(name)
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            DispatchQueue.main.async {
                if error != nil {
                    closure(DownloadResult.failure(name: name))
                } else {
                    if let data = data,
                        let image = UIImage(data: data) {
                        closure(DownloadResult.image(name: name, image: image))
                    }
                }
            }
        })
        task.resume()
    }
    
}
