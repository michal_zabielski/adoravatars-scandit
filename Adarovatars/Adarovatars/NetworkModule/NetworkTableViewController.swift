//
//  NetworkTableViewController.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import UIKit

private let reuseIdentifier = "NetworkCell"

class NetworkTableViewController: UITableViewController, NetworkViewDelegate {

    var viewModel: NetworkViewModelType?
    
    func reloadAll() {
        self.tableView?.reloadData()
    }
    
    func updateAt(index: Int) {
        self.tableView?.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadAll()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.items.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        if let cell = cell as? NetworkTableViewCell,
            let item = viewModel?.items[indexPath.row] {
            cell.setup(item: item)
        }

        return cell
    }
}
