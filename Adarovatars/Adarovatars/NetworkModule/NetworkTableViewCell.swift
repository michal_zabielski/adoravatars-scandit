//
//  NetworkTableViewCell.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import UIKit

class NetworkTableViewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTimestamp: UILabel!
    
    func setup(item: NetworkViewItem){
        lblName.text = item.name
        lblStatus.text = item.status
        lblTimestamp.text = item.timestamp
    }

}
