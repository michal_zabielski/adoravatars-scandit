//
//  NetworkModuleTypes.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation

enum NetworkStatus: String {
    case failure = "Failure"
    case success = "Success"
    case inProgress = "In Progress"
}

struct NetworkViewItem {
    let name: String
    let status: String
    let timestamp: String
}

protocol NetworkViewDelegate: class {
    func reloadAll()
    func updateAt(index: Int)
}

protocol NetworkViewModelType {
    var items: [NetworkViewItem] { get }
    var viewDelegate: NetworkViewDelegate? { get set }
    func reloadAll()
}
