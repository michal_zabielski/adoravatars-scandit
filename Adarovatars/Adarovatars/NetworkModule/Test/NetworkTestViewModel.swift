//
//  NetworkTestViewModel.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation

class NetworkTestViewModel: NetworkViewModelType {
    var coordinator: CoordinatorType?
    var items: [NetworkViewItem] = []
    weak var viewDelegate: NetworkViewDelegate? {
        didSet {
            viewDelegate?.reloadAll()
        }
    }
    
    init() {
        reloadAll()
    }
    
    func reloadAll() {
        items = []
        
        let loaded = Constants.names[1...10].map { name in
            NetworkViewItem(name: name, status: "Loaded", timestamp: NSDate().description)
        }
        
        let loading = Constants.names[11...15].map { name in
            NetworkViewItem(name: name, status: "Loading", timestamp: NSDate().description)
        }
        
        let empty = Constants.names[16...20].map { name in
            NetworkViewItem(name: name, status: "Failed", timestamp: NSDate().description)
        }
        
        items.append(contentsOf: loaded)
        items.append(contentsOf: loading)
        items.append(contentsOf: empty)
        
        viewDelegate?.reloadAll()
    }
}
