//
//  NetworkViewModel.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation

private var dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .none
    formatter.timeStyle = .medium
    
    return formatter
}()


class NetworkViewModel: NetworkViewModelType {
    var coordinator: CoordinatorType?
    var downloaderService: DownloaderService
    var items: [NetworkViewItem] = []
    weak var viewDelegate: NetworkViewDelegate? {
        didSet {
            viewDelegate?.reloadAll()
        }
    }
    
    init(downloaderService: DownloaderService) {
        self.downloaderService = downloaderService
        reloadAll()
    }
    
    func indexOfItem(named: String) -> Int? {
        return items.index(where: {
            $0.name == named
        })
    }
    
    func reloadAll() {
        items = downloaderService.itemsState.map { item in
            NetworkViewItem(downloadResult: item)
        }
        viewDelegate?.reloadAll()
        
        downloaderService.addListener { [weak self] result in
            guard let strongSelf = self else { return }
            let name = result.name
            if let index = strongSelf.indexOfItem(named: name) {
                strongSelf.items[index] = NetworkViewItem(downloadResult: result)
                strongSelf.viewDelegate?.updateAt(index: index)
            }
        }
    }
}

private extension NetworkViewItem {
    
    init(downloadResult: DownloadResult){
        let currentDate = dateFormatter.string(from: Date())
        switch(downloadResult){
        case .failure(let name):
            self.init(name: name, status: NetworkStatus.failure.rawValue, timestamp: currentDate)
        case .image(let name, _):
            self.init(name: name, status: NetworkStatus.success.rawValue, timestamp: currentDate)
        case .inProgress(let name):
            self.init(name: name, status: NetworkStatus.inProgress.rawValue, timestamp: currentDate)
        }
    }
}
