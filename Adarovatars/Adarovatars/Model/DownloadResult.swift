//
//  DownloadResult.swift
//  Adarovatars
//
//  Created by Michal Zabielski on 09.08.2017.
//  Copyright © 2017 mz. All rights reserved.
//

import Foundation
import UIKit.UIImage

enum DownloadResult {
    case image(name: String, image: UIImage)
    case failure(name: String)
    case inProgress(name: String)
    
    var name: String {
        switch (self) {
        case .failure(let name):
            return name
        case .image(let name, _):
            return name
        case .inProgress(let name):
            return name
        
        }
    }
}
